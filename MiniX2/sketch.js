function setup() {
  createCanvas(1000, 500);
  frameRate(10)
}

function draw() {
  background(242, 200, 242);
  textSize(50);
  strokeWeight(5);
  fill(random(100, 200), random(100, 200), random(100, 200));
  text("Hover over you emotion!", 190, 100);

  push();
  // **Draw face HAPPY**
  noStroke();
  fill(250, 200, 70); // Yellow color for the face
  ellipse(200, 300, 300, 300); // Face

  //LEFT EYE
  fill(100);
  ellipse(150, 260, 45);

  //RIGHT EYE
  ellipse(width - 750, 260, 45);

  //EYE CUT
  fill(250, 200, 70);
  ellipse(150, 270, 45);
  ellipse(width - 750, 270, 45);
  pop();

  if (mouseX < 350) {
    push();
    noStroke();
    // blush
    fill(255, 126, 179, 127);
    ellipse(100, 300, 80, 40)
    ellipse(300, 300, 80, 40)
    pop();

    push();
    noFill();
    // happy mouth
    stroke(0); // Black color for the mouth
    strokeWeight(4); // 
    arc(200, 350, 150, 100, 0, PI);
    pop();
  }

  push();
  // **Draw face SAD** 
  noStroke();
  fill(182, 208, 226); // color of the face
  ellipse(700, 300, 300, 300); // Face

  //RIGHT EYE
  fill(100);
  ellipse(640, 250, 45);

  //LEFT EYE
  ellipse(width - 245, 250, 45);

  //EYE CUT
  fill(182, 208, 226);
  ellipse(640, 240, 45);
  ellipse(width - 245, 240, 45);
  pop();

  if (mouseX > 550) {
    push();
    noStroke();
    // tears
    fill(135, 206, 235, 127);
    rect(620, 265, 45, 140);
    rect(735, 265, 45, 140);
    pop();

    push();
    // sad mouth
    noFill();
    stroke(0); // Black color for the mouth
    strokeWeight(4);
    arc(700, 390, 150, 100, PI, 0); // Arc for the smile
    pop();
  }


}
