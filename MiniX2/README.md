> # MiniX2: Choose Your Emotion

![Alternativ Tekst beskrivelse af billedet](MiniX2Screen.png "Choose your emotion")

<br>

Please run the code [here](https://andrea-ap.gitlab.io/aesthetic-programming/MiniX2/)

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->

<br>

Please view my whole MiniX folder [here](https://gitlab.com/andrea-ap/aesthetic-programming/-/tree/main/MiniX2)

### **About my MiniX**


For this MiniX program I have created 2 emojis. The user can choose to hover over the emoji which symbolises an emotion: happy and sad. Before the user hovers over one of the emojis they are both blank. However when the user hovers over the yellow emoji, blush and a happy smile appear. Likewise when the user hovers over the blue emoji, tears and a sad smile appear. 
<br>
To create this I have mainly used circles, ellipses and rectangles. 
I also learned how to use the pop and push functions, and implemented them within my code to have more control over my formatting. I also decided to use an "if" function that surveys the canvas and if the users mouse goes into a certain area, it displays the emotion. Another new tool i used was "Arc" and PI to create the smile itself. 
<br>
# Social context
How would you put your emoji into a wider social and cultural context that
concerns a politics of representation, identity, race, colonialism, and so on? (Try to think through the assigned reading and your coding process, and then expand that to your own experience and thoughts - this is a difficult task, you may need to spend some time thinking about it). 

With my emoji i tried to keep it as simple as possible. The sad emoji has the background color blue, which i think is very neutral and clearly symbolises sadness. The phrase "are you feeling blue" is common and showcases the cultural meaning of blue. The yellow emoji is also neutral. The original emoji is yellow and serves as a "base" color for many emojis today, and is a way to make them neutral instead of having to choose a skin color. I therefore picked yellow to stay unpolitical. Furthermore, my emojis do not symoblise gender, as i believe gender within emojis is unnecessary. Emojis should serve the sole purpose of conveying an emotion - what does that have to do with gender? Nothing. 
<br>
In my opinion there are way too many emojis that don't have any purpose really. I can still discover new emojis i have never used, even after using iPhone since forever. 
I know many people take offence to certain emojis because of politics, identity, race, gender, colonialism, and so on. But sometimes it just seems like we are in an age of taking offence, where everyhting becomes macro studied - and people decide to take offence on account of others (who may not even have been offended in the first place). However, of course it is important to design emojis that are not racist, sexist, homophobic and so on. Design should be inclusive, but have we perhaps overcomplicated the need for an emoji design of EVERYTHING? My emojis can be placed in neutral political ground. If anyone becomes offended by these two faces I'll be surprised. 
