>  # MiniX6: Game

  

  

![Arctic](MiniX6Screen.png "icy")

  

<br>

  

  

Please run the code [here](https://andrea-ap.gitlab.io/aesthetic-programming/MiniX6/)

  

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->

  

<br>

  

Please view the full repository [here](https://gitlab.com/andrea-ap/aesthetic-programming/-/tree/main)


## **About my MiniX**

  

- **Describe**  how does/do your game/game objects work?

  

My MiniX6 is an interpretation of the Chrome “dinosaur game” that appears when there is no wifi. In my version, the character is a little pink penguin, and the object that it has to avoid is smiling iceberg. The background of the game is beautiful icy/arctic landscape with snow-covered mountains and an icy lake. The penguin is static in its position by the left corner of the canvas, while the smiling icebergs are in motion. The icebergs move from outside the right side of the canvas and towards and past the pink penguin character. The goal of the game is thereby for the player (who acts as the penguin), to use the spacebar to `jump`, and avoid the icebergs. If the penguin and the iceberg collide, the game is over.

  <br>

  

- **Describe**  how you program the objects and their related attributes, and the methods in your game.

  

  

In my code I declare `let penguin` in the first lines of my sketch.js file. In line 14 I then call:

```
function setup() {
	createCanvas(900, 450);
	penguin = new Penguin();
}
```

This allows me to use `penguin` to access the methods and attributes defined in the `Penguin` class - and thereby `penguin` becomes an **object** of the `Penguin` class. Within the Penguin class, I use several attributes and methods. Here I will list them:

  

**Attributes:**

-   **this.r**: Represents the size (radius) of the penguin.
-   **this.x**: Represents the x-coordinate of the penguin's position.
-   **this.y**: Represents the y-coordinate of the penguin's position.
-   **this.vy**: Represents the velocity of the penguin in the vertical direction.
-   **this.gravity**: Represents the gravitational force affecting the penguin's vertical movement.

  

**Methods:**
-   **constructor()**: Initializes the penguin object with initial values for size (`r`), position (`x and y`), vertical velocity (`vy`), and gravitational force (`gravity`).
-   **jump()**: Allows the penguin to jump. It checks if the penguin is on the ground (at the bottom of the canvas), and if so, it sets the vertical velocity (`vy`) to a negative value to make the penguin jump upwards.
-   **hits(ice)**: Checks if the penguin collides with an object representing ice. It uses the `collideRectRect()` function from p5.js to detect collision between rectangles.
-   **move()**: Updates the position of the penguin based on its vertical velocity (`vy`) and applies gravitational force (`gravity`). It also ensures that the penguin stays within the bounds of the canvas.
-   **show()**: Displays the penguin on the canvas using the `image()` function from p5.js. It takes an image (`pImg`) and draws it at the penguin's position (`x and y`) with the specified size (`r`).

<br>

To exemplify the use of `penguin` as an object, we can first look at my penguin.js file. In line 11 of my penguin.js file I call the jump method:

```
jump() {
	if (this.y == height - this.r) {
	this.vy = -35;
	}
}
```

These lines of code check if the penguin’s current y-coordinate (`this.y`) is equal to the height of the canvas minus the penguin's radius (`height - this.r`). This condition ensures that the penguin is at the bottom of the canvas before allowing it to jump.  In the second line, `this.vy = -35;` : If the condition in the `if` statement is true (meaning the penguin is at the bottom of the canvas), this line sets the penguin's vertical velocity (`vy`) to -35. This value gives the penguin an upward velocity, causing it to jump upwards.

If we then switch to my sketch.js file, in line 20 I call the following function:

```
function keyPressed() {
	if (key == ' ') {
	penguin.jump();
	}
}
```

Here I then use the `jump()` method declared in the `Penguin class` to make the `penguin` object jump in response to the users interaction with the space bar key. This was just a highlight of one example using the object in my game, there are of course more. 

<br>

- Draw upon the assigned reading,  **what**  are the characteristics of object-oriented programming and the wider implications of abstraction?

  

  

Object-oriented programming (OOP) is a programming paradigm based on the concept of "objects," which can contain data in the form of attributes and code in the form of methods or functions. One of the main themes in the chapter about OOP is abstraction. As they write on page 145, “_The main goal is to handle an object’s complexity by abstracting certain details and presenting a concrete model._”. Abstraction thereby is the act of leaving out certain details to for example make it easier to read code as a human. Although it can appear/be used in many layers and scales of computing. “_Certain attributes and relations are abstracted from the real world, whilst simultaneously leaving details and contexts out._” (146). The simplification of complex objects can happen through abstraction: “_The understanding is that objects in the real world are highly complex and nonlinear, such abstracting and translating processes involve decision making to prioritize generalization while less attention is paid on differences._” (147). The wider implications of abstraction can therefore be that projects becomes more manageable and easier to understand. It can also facilitate collaboration among team members, as developers can work on different aspects of the game without needing intimate knowledge of each other's code implementations.

  
<br>
  

- **Connect**  your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?

  

This game could perhaps be connected to a wider cultural context relating to environmental conservation and challenges that animals face due to human activities. Obviously the world is dealing with climate change (to some extent, not enough if you ask me), however many animals are trying to survive in their drastically changing environments. So if this was the basis of my game, we could say that the rising temperature being caused by climate change, is abstracted in my game into the icebergs, that then could represent melting ice in the poles.

<br>

## **References**

TheCodingTrain Coding Challenge #147: [view here](https://www.youtube.com/watch?v=l0HoJHc-63Q)
<br>
This p5 library called p5.collide2D: https://github.com/bmoren/p5.collide2D 