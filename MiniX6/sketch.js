let penguin;
let pImg;
let iImg;
let bImg;
let ice = [];
let gameOver = false;

function preload() {
    pImg = loadImage('penguin.png');
    iImg = loadImage('ice.png');
    bImg = loadImage('background.jpg');
}

function setup() {
    createCanvas(900, 450);
    penguin = new Penguin();

}

function keyPressed() {
    if (key == ' ') {
        penguin.jump();
    }
}

function draw() {
    if (random(1) < 0.005) { //number of icebergs 
        ice.push(new Ice());
    }
       
    background(bImg);

    for (let i of ice) {
        i.move();
        i.show();
        
        if(penguin.hits(i)){
            console.log('game over');
            noLoop();
            textSize(50); 
            textStyle(BOLD); 
            strokeWeight(5);
            fill('#FF69B4');
            textAlign(CENTER, CENTER); // Align text to center
            text("Game Over!", width / 2, height / 2); // Draw "Game Over!" text in the center of the canvas
        }
    }

   
    penguin.show();
    penguin.move();

   
}
  





