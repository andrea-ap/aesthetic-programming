> # MiniX1: Desert Sketch

<br>

Run the code [here](https://Andrea-ap.gitlab.io/aesthetic-programming/MiniX1)

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->

<br>

View my MiniX1 repository [here](https://gitlab.com/andrea-ap/aesthetic-programming/-/tree/main/MiniX1?ref_type=heads)

<br>
![Alternativ Tekst beskrivelse af billedet](MiniX1Screen.png "Desert Sketch")
<br>

### **About my MiniX**

For my MiniX1 I have produced a canvas containing a desert scene. There is a pyramid in the middle, sand as the base, blue sky and two cacti, each with a small flower. In the top quarter of the canvas there is also a sun that travels from left to right across the x axis of the canvas. To create the pyramid I have used the function "triangle()" with a string of values that represent its placement on the x and y axis. To create the sun and i used the function "circle(sunX, sunY, 50)", and in the following line i defined the variable "sunX=(sunX + 0.5) % width" which increases the circles value on the x axis by 0.5, causing the circle to move to the right. The sand bottom and the cacti are made using rect() with different values. 
<br>

### **Reflection**
My first independent coding experience has been a good learning experience. Although it has been mainly difficult and frustrating, towards the end of this project I felt more confident and comfortable using p5.js. I definitely enjoy using p5 more than html and javascript because it seems like it’s much quicker (and easier) to produce “nice” looking sketches. I also find it a lot simpler to decode/read.
<br>
The coding process is similar to reading and writing a text in the way that all of the commands have meaning. You are essentially writing a text in a format that the computer will understand. Like an author writes a book in a format that we understand when we read. We therefore learn to understand the code that we write so we know what we are telling the computer to do.
<br>
What does code and programming mean to me? To me coding and programming is a tool of expression. With the use of p5 it has become a tool for more creative and artistic expression, while html and javascript acted more as expressions in the mathematical sense - that could solve problems like finding prime numbers etc. Although I find it really difficult to find sense and logic in all of the coding we are learning, it is beginning to make more sense, and I feel much more confident and intrigued with all of the new possibilities I can see with creative coding. This first project makes me excited for all of the coming miniXs, and what I might create to solve the assignment. My miniX1 is a very simple depiction of a nature scene, I can only imagine that the following projects will be more interactive and fun.
<br>
The assigned reading helps with the coding process by providing a better understanding of some of the terms, as well as explanations of concepts and ways of doing things. By reading about new techniques we broaden our horizons and become inspired to learn even more tools and commands within p5.
<br>
Overall this has been an interesting miniX as it was the first of many, and an introduction to the rest of the semester.
