let sunX = 0;
let sunY = 70;

function setup() {
  createCanvas(500, 500)
}

function draw() {
  // himmel og sand
  background(90, 170, 250);
  fill(255, 253, 208);
  stroke(255, 253, 208);
  rect(0, 370, 500, 130);

  // pyramide
  fill(248, 222, 126);
  stroke(248, 222, 126)
  triangle(60, 390, 470, 390, 250, 130);

  // sol + bevægelse
  fill(252, 226, 5);
  noStroke()
  circle(sunX, sunY, 50)
  sunX = (sunX + 0.5) % width

  // kaktusser
  fill(0, 128, 0);
  rect(30, 335, 20, 130, 20);
  rect(55, 355, 20, 60, 40);
  rect(40, 400, 35, 20, 20);
  rect(5, 350, 20, 50, 20);
  rect(5, 380, 40, 20, 40);
  rect(320, 400, 35, 35, 13);

  // blomster
  fill(202, 31, 123);
  circle(345, 400, 7);
  circle(68, 355, 7);
}
