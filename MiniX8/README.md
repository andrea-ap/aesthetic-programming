# TOTALLY REAL ED SHEERAN QOUTES

  

  

![GIF](edReadMe.gif  "Ed")

  

<br>

  

  

Please run the code [here](https://andrea-ap.gitlab.io/aesthetic-programming/MiniX8/)

  

  

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->

  

  

Please view the full repository [here](https://gitlab.com/andrea-ap/aesthetic-programming/-/tree/main)

  

<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

## **About Our MiniX**

#### What is the program about? Which API have you used and why?
Our program displays three main elements: Ed Sheeran, a button and a generated quote. The program also plays a remixed version of Shape of You, in the background. The purpose of this miniX is solely to display these Kanye quotes in the face of Ed Sheeran while listening to a homemade cover of his song.
    
The API we used is a “Kanye West quotes” api, that randomly displays different quotes Mr.West has given. We decided to use this API because we thought it was fun and different. We also really liked the element of surprise that the randomness of the generated quote creates. Kanye West is known to have said MANY controversial things, and is just very present online, especially on “X”.

#### Can you describe and reflect on your process in this miniX in terms of acquiring, processing, using, and representing data? How much do you understand this data or what do you want to know more about? How do platform providers sort the data and give you the requested data?  What are the power relations in the chosen APIs? What is the significance of APIs in digital culture?


Our process of acquiring this API was very simple. We found it online after searching for “free apis”, and copied the URL into our sketch.js file. The data in this API are the Kanye West quotes. These are represented in our miniX when they are displayed on the canvas. The user is able to press the button in the center of the canvas which refreshes the page and displays a new quote.
    
We understand this data very easily, as the quotes are very readable and the API online was easy to understand.
    
APIs are of great significance in our digital culture. They connect interfaces and help designers easily add new features to their websites. APIs make it easier for companies to work together. An example from real life is the ability to pay with MobilePay when online shopping on a majority of Nordic websites.

There aren’t really any power relations in this API - it consists only really of an array and a string of quotes.   
 
The API comes from a JSON file that contains an array with a string of quotes

#### Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time
How do the “web crawlers”/bots look through the web and collect data that then can be used in APIs?

  
  


