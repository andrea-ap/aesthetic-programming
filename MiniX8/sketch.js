// 
let quote;
let backgroundImage;
let edYapping;
let edSound;
let font;

// preloade all images so the website runs smooth
function preload() {
  // the ed gif
  edYapping = loadImage('edgif.gif')
  //the music
  edSound = loadSound('ed.mp3');
  soundFormats('mp3', 'ogg');
  //the font for the text
  font = loadFont('font.ttf');
  // the background image
  backgroundImage = loadImage('divideBackground.png')
}

// function for the API
function refreshData() {
  // loads the API by using a JSON file and picks a random string (quote) form the list
  loadJSON('https://api.kanye.rest', data => {
    quote = data.quote;
  });
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  // load the first qoute form the API
  refreshData();
  // creates a button the user can intereact with
  let refreshButton = createButton('GET NEW <br> ED SHEERAN QUOTE');
  // the button is styled in the styles.css file
  refreshButton.position((width - refreshButton.width) / 2, windowHeight / 2 - 20);
  // when pressed the button wil show a new quote
  refreshButton.mousePressed(refreshData);

  // makes the music loop, so i never stops (we fucking love ed sheeran yippie!!!)
  edSound.loop()

}

function draw() {
  // the background image and ed shareen image positions
  image(backgroundImage, 0, 0, windowWidth, windowHeight);
  image(edYapping, windowWidth / 1.75, 0, windowHeight, windowHeight)

  // the quote graphics and position
  fill('yellow');
  textFont('font');
  textSize(windowWidth / 40);
  textAlign(CENTER, CENTER);
  // checks if a quote is loaded and shown. If not, it shows the error message. 
  if (quote) {
    text(quote, windowWidth / 12, windowHeight / 8, 600, 600);
  } else {
    text("AINT GON HAPPEN 🗣️‼️", windowWidth / 12, windowHeight / 8, 600, 600);
  }
}

