> # MiniX3: Throbber

  

![Alternativ Tekst beskrivelse af billedet](MiniX3Screen.png  "Throbber")

<br>

  

Please run the code [here](https://andrea-ap.gitlab.io/aesthetic-programming/MiniX3/)

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->

<br>

Please view the full repository [here](https://gitlab.com/andrea-ap/aesthetic-programming/-/tree/main)
### **About my MiniX**

My MiniX3 is made in a 400x400 canvas with a light blue background. In its center a yellow sun rotates around itself slowly. In a ring around the sun, the text "Enjoy some sun during the buffer!" is curved around the sun and rotates in the opposite direction. Together these two elements complete the throbber as a whole. 
<br>
I have created this throbber to counter the negative associations we usually have with throbbers. They are often referred to as "spinning balls of doom" and are associated with long buffering and glitches in UI. The throbber I have created is a sun, and asks the user to enjoy it while the buffer is, well buffering. The sun acts as a symbol for positivity and hope. In real life, the sun literally gives light and life to everything on Earth. So it's safe to say that generally, everyone loves the sun:))

<br>
    
-   What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?

One type of time-related syntax that I have used in my code is the "frameCount" function- which i used in my "rotate()" function and "draw()" function. This was important for me to use as this function keeps track of the number of frames rendered since the program started. I use it to calculate the rotation, the function therefore ensures that the rotation angle continuously increases over time, which is what gives it the spinning effect.
Time is being constructed in computing in several ways. It has always been important for us humans to mirror our realities in computation. And time is an essential part of being human. Time awareness follows us from we are born and until we die, we always have a sense of how long things take (or will take). In computing we have mirrored this. When downloading files, a computer might display a slider that gradually becomes filled until it reaches "100%" downloaded - giving us a sense of how much more time it will take.  On page 93 of Aesthetic Programming, the authors similarly write, *"This looped deferral of ending is ontologically exacerbated with computation, unfolding the ending of being as a time-critical condition for both humans and machines alike. Leaving aside a deeper discussion of Heidegger’s philosophy, the importance of this for the discussion of loops seems to mirror the complexity of lived time."*.  Here the authors point out that the computing seems to mirror even small aspects of our complex human lives and our sense of time. 
We use throbbers to let users know that the computer is working on completing the task they just initiated, and that they must wait (instead of the screen remaining unchanged until the task is "invisibly" completed). This is also an example of constructed time, as the computer is programmed to show this in order to accommodate a humans perception of time. 
<br>
    
-   Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? 

When I think of a throbber I always think of Apple's round throbber with different colors spinning super fast. To me this is the OG / classic throbber. I think this throbber is supposed to be "happy" or at least give off good "vibes" with its very vibrant expression. However, I just have endless amounts of bad associations with this particular throbber because i have experienced so many crashes with it visible on my screen - unable to do anything. This throbber hides everything. You are unable to see or know what is going on, and the spinning effect of the circle makes you feel like it will go on forever, and never stop loading. I much prefer some sort of scale where you are able to see how much more buffering you have in front of you - until you reach "100%" completion of the load. 