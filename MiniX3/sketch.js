
let sentence = "Enjoy some sun during the buffer! ";
let sentenceArray = [];
let r = 130; // radius
let theta = 0;
let numRays = 18; // Number of rays
let angle = 0;
let diameter = 100;

function setup() {
  createCanvas(400, 400);
  textAlign(CENTER);
  textSize(18);
  fill(255);

  sentenceArray = sentence.split(""); // splits a string into an array of chars
  
  console.log(sentenceArray);
}

function draw() {
  background(173, 216, 240); //blue background
  translate(width / 2, height / 2);
  rotate(radians(frameCount)/10);
  for (let i = 0; i < sentenceArray.length; i++) {
    rotate(QUARTER_PI/(sentenceArray.length * 0.125)); // rotation for the group of letters, which affects the spacing between letters
    push();
    translate(r  * sin(theta), r * cos(theta));
    rotate(PI); // rotation for individual letter
    text(sentenceArray[i], 0, 0);
    pop();
  }
  
  // sun throbber rays 
  push();
  rotate(-angle); //rotating towards the left
  drawThrobber(diameter, numRays);
  angle += 0.02; //speed of rotation rays
  pop();
  
  //center of the sun 
  push();
  noStroke();
  fill(255, 255, 51);
  ellipse(0, 0, 95);
  pop();
}

function drawThrobber(d, n) {
  stroke(255, 255, 51);
  strokeWeight(4);
  noFill();
  
  //calculating the x and y coordinates of the start/finish of the rays 
  
  // Calculate the angle between each ray
  let angleIncrement = TWO_PI / n;
  
  // Loop through each ray and draw it
  for (let i = 0; i < n; i++) {
    
    // Calculate the angle for this ray
    let angle = angleIncrement * i;
    
    // Calculate the start and end points of the ray
    let x1 = cos(angle) * d / 2;
    let y1 = sin(angle) * d / 2;
    let x2 = cos(angle) * d;
    let y2 = sin(angle) * d;
    
    line(x1, y1, x2, y2) //drawing the rays of the throbber
  }
}



