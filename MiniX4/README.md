# MiniX4: Cookies

  

  

![Alternativ Tekst beskrivelse af billedet](MiniX4Screen.png  "Start Screen")

  
NOTE that the program contains loud sound, and only runs once. Refresh to go back to "start". 
  

<br>

Please run the code [here](https://andrea-ap.gitlab.io/aesthetic-programming/MiniX4/)

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->

<br>

Please view my whole MiniX folder [here](https://gitlab.com/andrea-ap/aesthetic-programming/-/tree/main/MiniX4)


<!-- change to the miniX folder -->

  

### **About my MiniX**



- Provide a title for and a short description of your work (1000 characters or less) as if you were going to submit it to the festival.

  

The name of my MiniX4 is _Cookies_. This MiniX uses satire to explore the ethical implications of 'cookies', a topic that demands our attention every time we navigate to a new web domain. Cookies provide website owners access to an abundance of user data accumulated during online visits. Regrettably, many users unwittingly consent to these potentially compromising cookies. Opting for the blanket 'yes to all' is often more convenient than meticulously deselecting specific cookie types, impacting our browsing experience. As a result, cookies have become an involuntary aspect of our daily online routine, despite our limited understanding of the data they collect and how it might affect us later.

- Describe your program and what you have used and learnt.

The program starts by asking the user the question, "Do you accept my cookies?", and allowing the answers "Accept" and "Decline". You are therefore asked to make a choice on your preferences. If the user selects "Decline", the background changes to an image of a creeper in a cave. It looks sad and lonely, and the text now writes that they cannot be shown anything without cookies and they must choose to accept. When the user than goes back to the starting page and click on the "Accept" button, the background quickly changes to a picture and sound of lightning for a few seconds, and then proceeds to change again, to a picture of an angry looking character and the words "Herobrine has stolen all your data!".

I have learned a lot by creating this miniX. The use of both photos and audio in p5.js was new to me, and I struggled a bit to get it up and running for this program. But I learned. Something I got to practice was using if() and else statements. I used this several times in my code as a "check" for the program to know when to switch backgrounds, text, and button visibility. I also practiced using arrays. It has been a while since I have used them, and I believe it's the first time I use an array in my MiniXs. Something very useful I learned was controlling the background, button's, and text's placement when the canvas may be resized or opened in a different size by the user. To do this I defined variables for the X and Y positions of buttons and text, and afterwards gave these variables values.

  

- Articulate how your program and thinking address the theme of “capture all.” – What are the cultural implications of data capture?

  

The cultural implications are that you truly don’t know what you are saying yes to by accepting cookies on the internet. The lack of transparency online creates an unsafe environment where it is easy for consumers to get taken advantage of. In reality “cookies” serve as a form of currency. It’s the price we pay to access web domains for “free”. But is it really free if we are paying with our own data? Perhaps not. We don’t always know what the owners data capture is retrieving about us.. In the case of my MiniX, the data capture is mouse capturing. Collection of data regarding how the consumer uses their mouse and what decisions they are making with it.