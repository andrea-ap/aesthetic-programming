
let backgroundImage; // Variable to hold the current background image
let images = []; // Array to store multiple background images
let currentIndex = 0; // Index of the current background image
let nextChangeTime = 0; // Time when the next background change should occur
let showButton = true; // Flag to control the visibility of the button
let timer = 0; // Timer to track the duration of the lightning image
let button1;
let button2;
let sound;

// Define variables for element positions and sizes
let button1X, button1Y;
let button2X, button2Y;
let textX, textY;

function soundLoaded() {
    console.log("Sound loaded successfully");
    sound.play();
}

function preload() {
    // Load your images into the array
    images.push(loadImage('kitchen.png'));
    images.push(loadImage('lightning.jpg'));
    images.push(loadImage('herobrine.jpg'));
    images.push(loadImage('creeper.png'));

    sound = loadSound('lightning.mp3', soundLoaded);
}




function setup() {
    createCanvas(windowWidth, windowHeight);
    // Set the initial background image
    setBackgroundImage(images[currentIndex]);

    // button and text positions
    button1X = width / 2 - 200;
    button1Y = height - 40;
    button2X = width / 2 + 20;
    button2Y = height - 40;
    textX = width / 2;
    textY = 50;

    // Button to cycle through images
    button1 = createButton('💚ACCEPT💚');
    button1.mousePressed(nextImage);

    button2 = createButton('💔DECLINE💔')
    button2.mousePressed(switchToCreeper); // Add event listener to button2

}

function draw() {
    // the current background image
    image(backgroundImage, 0, 0, windowWidth, windowHeight);


    // If showButton flag is true, display the button
    if (showButton) {
        button1.position(button1X, button1Y);
        button1.show();
        if (currentIndex === 0) {
            button2.position(button2X, button2Y);
            button2.show();
        } else {
            button2.hide(); // Hide button2 for lightning and Herobrine images
        }
    } else {
        button1.hide(); // Hide button1 during lightning and Herobrine images
        button2.hide(); // Hide button2 during lightning and Herobrine images
    }

    // Display text based on the current background image
    textSize(32);
    fill(255); // Set text color to white
    textAlign(CENTER, CENTER);

    if (currentIndex === 0) {
        // Text to display with kitchen background
        text("Do you accept my cookies?👉👈🍪🩷", textX, textY);
    } else if (currentIndex === 1) {
        // Text to display with lightning background
        text("OH NO!", width / 2, height / 2);
    } else if (currentIndex === 2) {
        // Text to display with herobrine background
        text("HEROBRINE HAS STOLEN ALL YOUR DATA!", width / 2, height / 2);
    } else if (currentIndex === 3) {
        // Text to display with creeper background
        text("Life is sad without cookies🥺", textX, textY);
        text("I can't show you anything if you don't accept", textX, textY + 50);
        text("Refresh to change your mind", textX, textY + 100);
    }

    // Logic to display the lightning image for 5 seconds
    if (currentIndex === 1) {
        timer++;
        if (timer >= 5 * 60) { // 5 seconds * 60 frames per second
            nextImage(); // Switch to the next image after 8 seconds
        }
    }

}
// controlling the positions of text/buttons centered when window is resized
function windowResized() {
    resizeCanvas(windowWidth, windowHeight);

    // Updated element positions based on new canvas size
    button1X = width / 2 - 100;
    button1Y = height - 100;
    button2X = width / 2 + 20;
    button2Y = height - 100;
    textX = width / 2;
    textY = 50;
}

function nextImage() {
    // Increment the index to select the next image
    currentIndex = (currentIndex + 1) % images.length;
    // Set the next background image
    setBackgroundImage(images[currentIndex]);

    // Reset timer
    timer = 0;

    // Hide the button when displaying the lightning image
    if (currentIndex === 1 || currentIndex === 2 || currentIndex === 3) {
        showButton = false;
    } else {
        showButton = true;
    }

    // Play sound when switching to certain images
    if (currentIndex === 1) {
        sound.play();

    }
}



function setBackgroundImage(img) {
    // Set the background image
    backgroundImage = img;
}

function switchToCreeper() {
    // Switch to the creeper image when button2 is pressed
    currentIndex = 3; // Index of the creeper image in the images array
    setBackgroundImage(images[currentIndex]);
    showButton = false; // Hide buttons when displaying the creeper image
}



