>  # MiniX5: Generative Art

  

  

![Alternativ Tekst beskrivelse af billedet](MiniX5Screen.png "Gradient")

  

<br>

  

  

Please run the code [here](https://andrea-ap.gitlab.io/aesthetic-programming/MiniX5/)

  

<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->

  

<br>

  

Please view the full repository [here](https://gitlab.com/andrea-ap/aesthetic-programming/-/tree/main)

### **About my MiniX**

**-   What are the rules in your generative program? Describe how your program performs over time?How do the rules produce emergent behaviour? What role do rules and processes have in your work?**

  

I have 2 rules in my generative program. The first rule randomly changes the thickness of the lines, and the second rule randomly changes the colours of the lines. Because of these changes, the program creates a wavy and smooth effect of colours changing and blending together within the frame. It continues to do so forever. The program is therefore very eye-catching and intriguing to look at because you can’t imagine how it will look in 10 seconds. And its smoothness and softness is delicate and nice to look at.

  

**The first rule is produced by:**

  
```
let thickness = map(noiseVal, 0, 1, minThickness, maxThickness);
```
  

This line of code uses the `map` function to translate values. The `noiseVal`variable has been declared earlier, and holds the values of the Perlin noise that creates the ‘motion’ of the lines. 0 and 1 are the ranges of the min and max of `noiseVal`, and `minThickness` and `maxThickness` are the min and max for the thickness variable. So the map function uses all of these values and creates a range within them- so the  `noiseVal` range and thickness range now correspond. This creates the varying thicknesses that change smoothly over the canvas.

  

**The second rule is produced by:**

  
```
let r = map(noiseVal, 0, 1, 10, 255);

let g = map(noiseVal, 0, 1, 100, 200);

let b = map(noiseVal, 0, 1, 210, 255);
```
  

This rule essentially acts the same as the previous one, but with different values and outputs. Here the `noiseVal` range and the color range of r, g, and b become proportionally correspondent to each other. This ensures that the random values create new random colours that smoothly change over the canvas.

  

**-   Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?**

  

  

The experience of creating this miniX by using rules and autonomy to ensure control in the creative output was very interesting. Creating generative art is new to me, and the concept in general is very interesting. It is very intriguing to watch generative art unfold in front of you, especially when you don’t know what is going to happen. The MiniX really serves to highlight how something can be “auto”. I gave very few commands in my code, and yet I have created something that is ever changing and evolving. With the rules I gave in my code, I was able to create control over what was going to happen, to some extent. My rules were based on “randomness” but the control was within what would happen in the randomness.