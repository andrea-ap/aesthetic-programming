let numLines = 400; // Number of lines
let lineSpacing = 400; // Spacing between each line
let yPosition = 80; // Y-coordinate for all lines
let minThickness = 25; // Minimum thickness of lines
let maxThickness = 25; // Maximum thickness of lines
let noiseOffset = 0; // Initial noise offset

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255);
  frameRate(28);
}

function draw() {
  background(255); // Clear the canvas
  
  // Draw lines with varying thickness and color
  for (let i = 0; i < numLines; i++) {
    let xStart = 50; // Starting x-coordinate for the first line
    let xEnd = width - 50; // Ending x-coordinate for the last line
    
    let spacing = (xEnd - xStart) / (numLines - 1); // Calculate the spacing between each line to evenly distribute them on the available space
    
    let x = xStart + i * spacing; // Calculates the x-coordinate for the current line being drawn with equal distance to the next
    
    // Perlin noise giving smooth and continuous variation to the values of the lines
    let noiseVal = noise(i * 0.1, noiseOffset); // Increment noise offset for motion
    
    // Rule 1: Randomly vary line thickness based on noise
    let thickness = map(noiseVal, 0, 1, minThickness, maxThickness);
    
    // Rule 2: Randomly change line color based on noise
    let r = map(noiseVal, 0, 1, 10, 255);
    let g = map(noiseVal, 0, 1, 100, 200);
    let b = map(noiseVal, 0, 1, 210, 255);
    
    strokeWeight(thickness);
    stroke(r, g, b);
    line(x, yPosition, x, yPosition + lineSpacing); // Draw the line
  }
  
  // Variation of motion pattern over time 
  noiseOffset += 0.01;
}