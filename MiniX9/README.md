 # MiniX9: Flowcharts

  

  
My flowchart on MiniX6 - the game. 
<br>
![Arctic](GameFlowchart.png "icy")




<br>

Group flowchart on idea #1: Outfit Generator
<br>
![FLOW](FitGenerator.png "fit")

<br>

Group flowchart on idea #2: Cow Clicker Game
<br>
![FLOW2](ClickerGame.png "cow")

  




<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES} -->

  

<br>

  

Please view the full repository [here](https://gitlab.com/andrea-ap/aesthetic-programming/-/tree/main)
